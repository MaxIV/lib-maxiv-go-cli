# cli

A tiny CLI application framework

## Example

*See the* `example` *folder for full code*

```
⇒  go run ./example --help
Name: mybin
Usage: mybin <sub-command>
Sub commands:
  version - Prints version and exits
```

```
⇒  go run ./example version
v1.0.0
```